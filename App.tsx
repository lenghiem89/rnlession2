import React from 'react';
import{Button, SafeAreaView, ScrollView, StatusBar, StyleSheet, Text, useColorScheme, View, TextInput, TouchableOpacity,} from 'react-native';
type HeadingProps ={
    title: string, color: string,
};
const Heading: React.FC<HeadingProps> = ({title, color}) => {
    return (
        <Text style={[styles.textlarge, {color: color}]}>{title}</Text>
    )};
type TextInputmanualProps ={
    title: string,
};
const TextInputmanual: React.FC<TextInputmanualProps> = ({title, }) =>{
    return(
        <TextInput style={[styles.textinput, ]} placeholder={title}></TextInput>
    )};
type ButtonProps = {
    title: string, color: string,
};
const ButtonManual: React.FC<ButtonProps> = ({title, color,}, event) => {
    return (
        <TouchableOpacity style={[styles.button, {backgroundColor:color}]} onPress={event.preventDefault}>
            <Text style={{fontWeight:"bold"}}>{title}</Text>
        </TouchableOpacity>
    )};
const App = () => {
    return(
        <View style={styles.all}>
            <Heading title="Yolo System" color="green" />
            <TextInputmanual title="Tên đăng nhập" />
            <TextInputmanual title="Mật khẩu" />
            <ButtonManual title="Login" color="purple"/>
            <Heading title="Or" color="green" />
            <ButtonManual title="Facebook" color="green" />
            <ButtonManual title="Google" color="red" />
        </View>
    )};

const styles = StyleSheet.create({
    textlarge: {
        backgroundColor: 'white', color: 'green', fontSize: 50, fontWeight:'bold', alignSelf:'center', padding: 0, 
    },
    textinput: {
        padding: 12, width: "100%", color: 'gray', borderColor: 'black', borderWidth: 0.2, borderRadius: 12, fontSize: 14, marginTop: 5, marginBottom: 5,
    },
    all: {
        padding: "3%", marginTop: "42%", backgroundColor:'white', borderWidth: 0.5, borderRadius: 20, marginBottom:"100%", marginLeft: 4, marginRight: 4, borderColor: "black",
    },
    button:{
        marginTop: 5, marginBottom: 0, padding: 10, width: "100%", alignItems: 'center', borderRadius: 24, borderColor:'black',
    }
});

export default App;