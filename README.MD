Download and extract or clone the project
At project directory, run:
$npm install
$npx react-native start

Then choose the platform you need to run:
Ios:
$cd ios
$pod install
$cd ..
$npx react-native run-ios

Android:
$npx react-native run-android
